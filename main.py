from dlgVista import DlgVista

import flet as ft
import pandas as pd
import numpy as np
import random as rd
import os 

class Controlador:
    """
    Esta clase es la encargada de llenar, cargar y jugar con los componentes de la clase dlgVista()

    Métodos
    -------

    __init__(page,vista)
        Método contructor de la clase,
        Se encarga de inicializar todas las variables,
        tanto logicas como para controlar el comportamiento de los componentes de la vista.
    
    init_components()
        Iguala y activa las variables logicas del controlador con los componentes de la vista u otras librerías.
    
    rand_data()
        Crea una base de datos aleatoria de 0s y 1s con las dimensiones capturadas en Items e Instancias.
    
    cont_bin()
        Devuelve un contador binario inverso.
    
    fill_table(val_col,val_row)
        LLena una tabla utilizando una base de datos de Pandas,
        devuelve Columnas y Filas.
    
    fill_table_conting()
        Llena la tabla de contingencia,
        devuelve Columnas y Filas.

    fill_table_confus()
        llena la tabla de Frecuencias,
        devuelve Columnas y Filas.
    
    fill_cbx_items(name_items)
        llena los ComboBox con los items ingresados.
    
    fill_lv_conting()
        Calcula y llena el listView de la tabla de contingencias,
        con las reglas de asociación.
    
    fill_clean_table()
        Limpia columnas y filas de una tabla,
        devuelve columnas y filas.
    
    vali_dupli_items()
        Valida items duplicados,
        devuelve: 
        False, si están duplicados,
        True si no lo están.
    
    on_file_picked(e: ft.FilePickerResultEvent)
        Abre el buscador de archivos.
    
    vali_extens()
        Valida la extención del archivo seleccionado.

    load_db()
        Carga la base de datos,
        usa vali_extends() para cargar el archivo,
        si es un archivo valido
            valida si No supera la cantidad maxima de items (8),
                llena la tabla principal.
            Si no, devuelve un error de cantidad de items
        Si no, devuelve un error de extención no valida
    
    cal_tb_conting()
        Calcula los valores de la tabla de contingencia,
        devuelve una lista de esos valores.
    
    cal_tb_confus()
        Calcula los valores de la tabla de frecuencias,
        devuelve dos matrizes:
            Matriz de valores enteros de frecuencias,
            Matriz de valores enteros de contingencia.

    cal_chi()
        Calcula el chi y determina las significacia de p-valor,
        llena los labels con la información.

    on_key()
        Función para habilitar atajos del teclado,
        se usa para agregar y validar los items ingresados.
    
    on_clic(btn)
        Función para activar funciones de los botones,
        recibe un String dependiendo del tipo de botón que acceda a la función.
    
    

    
        
    
    """

    def __init__(self,page=None,vista=None):
        self.page = page
        self.vista = vista
        self.n_items = 0
        self.n_inst = 0
        self.name = ''
        self.chi = ''
        self.sig95 = ''
        self.sig99 = ''
        self.sig99_99 = ''

        self.list_item = None
        self.list_conting = None
        self.list_confus = None
        
        self.tb_as = None
        self.tb_conting = None
        self.tb_confus = None

        self.btn_manual = None
        self.btn_auto = None
        self.btn_calcular = None
        #self.btn_show = None

        self.cbx_items_1 = None
        self.cbx_items_2 = None

        self.url_file = ''
        self.df = None

    def init_compontents(self):

        self.n_items = self.vista.txt_items
        self.n_inst = self.vista.txt_inst
        
        self.name = self.vista.txt_name
        self.chi = self.vista.lbl_chi
        self.sig95 = self.vista.lbl_sig95
        self.sig99 = self.vista.lbl_sig99
        self.sig99_99 = self.vista.lbl_sig99_99

        self.list_item = self.vista.lv_item
        self.list_conting = self.vista.lv_conting
        self.list_confus = self.vista.lv_confus

        self.tb_as = self.vista.tb_as
        self.tb_conting = self.vista.tb_conting
        self.tb_confus = self.vista.tb_confus
        
        self.btn_manual = self.vista.btn_manual
        self.btn_manual.on_click=lambda e: self.on_clic('btnManual')
        self.btn_auto = self.vista.btn_auto
        self.btn_auto.on_click=lambda e: self.on_clic('btnAuto')
        self.btn_calcular = self.vista.btn_calcular 
        self.btn_calcular.on_click=lambda e: self.on_clic('btnCalcular')
        #self.btn_show = self.vista.btn_show
        #self.btn_show.on_click=lambda e: self.on_clic('btnShow')

        self.cbx_1 = self.vista.cbx_items_1
        self.cbx_2 = self.vista.cbx_items_2
        
        self.file_picker = ft.FilePicker(on_result=self.on_file_picked)
        self.page.overlay.append(self.file_picker)

        self.df = pd.DataFrame()

    def rand_data(self):
        num_rows = int(self.n_inst.value.strip())
        num_cols = len(self.list_item.controls)
        random_numbers = np.random.randint(2, size=(num_rows, num_cols))
        self.df = pd.DataFrame(random_numbers, columns=[(name.value).upper() for name in self.list_item.controls])
        self.tb_as.columns, self.tb_as.rows = self.fill_table(self.df.columns.values,self.df.iloc[::])

    def cont_bin(self):
        arr_bin = []
        for i in range(2**2 -1,-1,-1):
            # Convierte el número a binario y elimina el prefijo '0b'
            binario = bin(i)[2:]
            # Formatea el binario para que tenga n bits, rellenando con ceros a la izquierda si es necesario
            binario = binario.zfill(2)
            arr_bin.append(binario)
        return arr_bin

    def fill_table(self,val_col,val_row):
        columns = []
        rows = []
        
        columns.append(ft.DataColumn(label=ft.Text('ID')))
        
        for item in val_col:
            columns.append(ft.DataColumn(label=ft.Text(item)))

        for i in range(val_row.shape[0]):
            
            cells = []
            cells.append(ft.DataCell(ft.TextField(value=str(i),**self.vista.config_txt(val=2))))
            for ii in val_row.iloc[i][:]:
                cells.append(ft.DataCell(ft.TextField(value=str(ii),**self.vista.config_txt(val=2))))

            rows.append(ft.DataRow(cells))

        return columns, rows
    
    def fill_table_conting(self):
        conting_values = [int(x) for x in self.cal_tb_conting()]

        name_col = self.cbx_1.value
        config_label = self.vista.config_label(size=12,width=50)
        columns = [
                ft.DataColumn(label=ft.Text('',**config_label)),
                ft.DataColumn(label=ft.Text(name_col,**config_label)),
                ft.DataColumn(label=ft.Text('~'+name_col,**config_label)),
                ft.DataColumn(label=ft.Text('Total',**config_label))
            ]

        name_row = self.cbx_2.value
        total01= conting_values[0]+ conting_values[1]
        total23= conting_values[2] + conting_values[3]
        total02= conting_values[0] + conting_values[2]
        total13= conting_values[1] + conting_values[3]
        Total_end = total02+total13
        rows = [
            ft.DataRow(cells=[
                ft.DataCell(ft.Text(name_row,**config_label)),
                ft.DataCell(ft.Text(conting_values[0],**config_label)),
                ft.DataCell(ft.Text(conting_values[1],**config_label)),
                ft.DataCell(ft.Text(total01,**config_label)),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('~'+name_row,**config_label)),
                ft.DataCell(ft.Text(conting_values[2],**config_label)),
                ft.DataCell(ft.Text(conting_values[3],**config_label)),
                ft.DataCell(ft.Text(total23,**config_label))
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('Total',**config_label)),
                ft.DataCell(ft.Text(total02,**config_label)),
                ft.DataCell(ft.Text(total13,**config_label)),
                ft.DataCell(ft.Text(Total_end,**config_label)),
            ]),
        ]
        return columns, rows

    def fill_table_confus(self):
        col = self.cbx_1.value
        row = self.cbx_2.value
        mx_val = self.cal_tb_confus()[0]
        config_label = self.vista.config_label(size=12,width=50)

        columns = [
                ft.DataColumn(label=ft.Text('',**config_label)),
                ft.DataColumn(label=ft.Text(col,**config_label)),
                ft.DataColumn(label=ft.Text('~'+col,**config_label)),
            ]

        rows = [
            ft.DataRow(cells=[
                ft.DataCell(ft.Text(row,**config_label)),
                ft.DataCell(ft.Text(mx_val[0][0],**config_label)),
                ft.DataCell(ft.Text(mx_val[0][1],**config_label)),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('~'+row,**config_label)),
                ft.DataCell(ft.Text(mx_val[1][0],**config_label)),
                ft.DataCell(ft.Text(mx_val[1][1],**config_label)),
            ])
        ]
        return columns, rows

    def fill_cbx_items(self,name_items):
        for name in name_items:
            self.cbx_1.options.append(ft.dropdown.Option(name))

        for name in name_items:
            self.cbx_2.options.append(ft.dropdown.Option(name))

    def fill_lv_conting(self):
        columns = [self.cbx_1.value,self.cbx_2.value]
        config_label = self.vista.config_label(size=12,width=50)
        values = [int(x) for x in self.cal_tb_conting()]
         
        t1 = values[0]+values[1]
        t2 = values[2]+values[3]
        t3 = values[0]+values[2]
        t4 = values[1]+values[3]
        total = t1 + t2
        t_all = [t1,t2,t3,t4,total]
        v_bin = self.cont_bin()
        t = 0
        col1 = 0
        col2 = 1
 
        for n in range(4):
            v0 = v_bin[n]
            v1 = v_bin[n]
            cb = values[n] * 100 / t_all[-1]
            cf = values[n] * 100 / t_all[t]
            self.list_conting.controls.append(ft.Text(f'Si ({columns[1]} = {v0[0]}) '+
                                                        f'Entonces {columns[0]} = {v1[1]} \n'+
                                                        f'Cb = {round(cb,2)}% \n'+
                                                        f'Cf = {round(cf,2)}% ({values[n]}/{t_all[t]})',**config_label))
            if n % 2:
                t +=1
        
        t = 2
        for n in [0,2,1,3]:
            if n == 1:
                t +=1
            v0 = v_bin[n]
            v1 = v_bin[n]
            cb = values[n] * 100 / t_all[-1]
            cf = values[n] * 100 / t_all[t]
            self.list_conting.controls.append(ft.Text(f'Si ({columns[0]} = {v0[0]}) '+
                                                        f'Entonces {columns[1]} = {v1[1]} \n'+
                                                        f'Cb = {round(cb)}% \n'+
                                                        f'Cf = {round(cf)}% ({values[n]}/{t_all[t]})',**config_label))

    def clean_table(self):
        columns = [
                ft.DataColumn(label=ft.Text('')),
                ft.DataColumn(label=ft.Text('')),
                ft.DataColumn(label=ft.Text('')),
            ]

        rows = [
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
        ]
        return columns, rows

    def vali_dupli_items(self):
        for data in self.list_item.controls:
            if self.name.value == data.value:
                return False
        return True

    def on_file_picked(self,e: ft.FilePickerResultEvent):
        selected_files = e.files
        if selected_files:
            file_path = selected_files[0].path
            
            try:
                self.url_file = file_path
                self.load_db()
                self.btn_calcular.disabled = False
                self.page.update()
            except ValueError as err:
                self.vista.create_dialog(title='No soported',txt=err)
                self.tb_as.columns, self.tb_as.rows = self.clean_table()

    def vali_extens(self):
        _, file_extens = os.path.splitext(self.url_file)

        if file_extens == '.csv':
            self.df = pd.read_csv(self.url_file)
        elif file_extens in ['.xls', '.xlsx']:
            self.df = pd.read_excel(self.url_file)
        elif file_extens == '.json':
            self.df = pd.read_json(self.url_file)
        elif file_extens == '.parquet':
            self.df = pd.read_parquet(self.url_file)
        elif file_extens == '.feather':
            self.df = pd.read_feather(self.url_file)
        else:
            self.df = pd.DataFrame()

        return file_extens

    def load_db(self):
        file_extens = self.vali_extens()
        if not self.df.empty :
            if len(self.df.columns.values) <= 8:
                self.tb_as.columns, self.tb_as.rows = self.fill_table(self.df.columns.values,self.df.iloc[::])
                self.fill_cbx_items(self.df.columns.values)
                self.page.update()
            else:
                raise ValueError (f"La longitud del archivo: {len(self.df.columns.values)} supera el limite de 8 columnas")
        else:
            raise ValueError(f"Formato de archivo no soportado: {file_extens}")

    def cal_tb_conting(self):
        A = self.cbx_1.value
        B = self.cbx_2.value

        ab = str(self.df[(self.df[A] == 1) & (self.df[B] == 1)].shape[0])
        a_b = str(self.df[(self.df[A] == 1) & (self.df[B] == 0)].shape[0])
        _ab = str(self.df[(self.df[A] == 0) & (self.df[B] == 1)].shape[0])
        _a_b = str(self.df[(self.df[A] == 0) & (self.df[B] == 0)].shape[0])

        return [ab,a_b,_ab,_a_b]
       
    def cal_tb_confus(self):
        val = [int(x) for x in self.cal_tb_conting()]
        mx_values =[
            [val[0],val[1],val[0]+val[1]],
            [val[2],val[3],val[2]+val[3]],
            [val[0]+val[2],val[1]+val[3], sum(val)],
        ]
       
        valAB = (mx_values[0][0] / 100) / ((mx_values[0][2] / 100) * (mx_values[2][0] / 100))
        valA_B = (mx_values[0][1] / 100) / ((mx_values[0][2] / 100) * (mx_values[2][1] / 100)) 
        val_AB = (mx_values[1][0] / 100) / ((mx_values[1][2] / 100) * (mx_values[2][0] / 100))
        val_A_B = (mx_values[1][1] / 100) / ((mx_values[1][2] / 100) * (mx_values[2][1] / 100))
        valAB = round(valAB,3)
        valA_B = round(valA_B,3)
        val_AB = round(val_AB,3)
        val_A_B = round(val_A_B,3)
        return [
            [valAB,valA_B],
            [val_AB,val_A_B],
        ], mx_values 

    def cal_chi(self):
        mx_val = self.cal_tb_confus()[1]
        o = []
        e = []
        for y in range(2):
            for x in range(2):
                o.append(mx_val[y][x])

        for y in range(2):
            for x in range(2):
                val = mx_val[y][2] * mx_val[2][x] / mx_val[2][2]
                e.append(val)
        #print(o) 
        #print(e)

        chi = []
        for x in range(4):
            chi.append(round((((o[x]-e[x])**2)/e[x]),3))

        #print(chi)
        chi = sum(chi) 

        self.chi.value += f'\n{chi}'
        if chi < 3.841:
            self.sig95.value += '\nNO' 
            self.sig99.value += '\nNO' 
            self.sig99_99.value += '\nNO'

        elif chi < 6.635:
            self.sig95.value += '\nYES' 
            self.sig99.value += '\nNO' 
            self.sig99_99.value += '\nNO'

        elif chi < 10.828:
            self.sig95.value += '\nYES' 
            self.sig99.value += '\nYES' 
            self.sig99_99.value += '\nNO'
        else:
            self.sig95 += '\nYES' 
            self.sig99 += '\nYES' 
            self.sig99_99 += '\nYES'

    def on_key(self,event):
        if event.key == 'Enter':
            try: 
                if not (self.vista.txt_items.value.strip() != '' and self.vista.txt_inst.value.strip() != ''):
                    self.vista.create_dialog(title='Items and Instant Void',txt='no deje vacio la cantidad de items e instancias')
                    self.vista.txt_name.value = ''
                    self.vista.txt_name.focus()
                else:

                    if not (0 < int(self.n_items.value.strip()) <= 8):
                        self.vista.create_dialog(title='Max 8 Items',txt='No puede superar los 8 items')
                        self.name.value = ''
                        self.name.focus()
                    else:
                        if not (len(self.list_item.controls) < int(self.n_items.value.strip())):
                            self.vista.create_dialog(title='Max Items',txt='No puede superar la cantidad de items')
                            self.name.value = ''
                            self.name.focus()
                        else: 

                            if not (self.vali_dupli_items()):
                                self.vista.create_dialog(title='Duplicate Items',txt='No puede ingresar un item dos veces')
                                self.name.value = ''
                                self.name.focus()
                            else: 
                                if self.name.value != '':
                                    self.list_item.controls.append(ft.Text(self.name.value,size=20,color=ft.colors.BLACK))
                                    self.name.value = ''
                                    self.name.focus()
                                    self.page.update()

            except ValueError as err:
                print('Error al ingresar items: ',err)
            
    def on_clic(self,btn):
        if btn == 'btnManual':
            if self.list_item.controls == []:
                self.vista.create_dialog(title='Manual no action',txt='Primero ingrese los nombres de los items')
            else:
                self.cbx_1.options.clear()
                self.cbx_2.options.clear()
                self.rand_data()
                items_list = [(item.value).upper() for item in self.list_item.controls]
                self.fill_cbx_items(items_list)
                self.list_item.clean()
                self.n_items.value = ''
                self.n_inst.value = ''
                self.name.value = ''
                self.btn_calcular.disabled = False
                self.page.update()
                
        if btn == 'btnAuto':
            self.cbx_1.options.clear()
            self.cbx_2.options.clear()
            self.file_picker.pick_files(allow_multiple=False)
        
        if btn == 'btnCalcular':
            if not (self.cbx_1.value and self.cbx_2.value):
                self.vista.create_dialog(title='Cbx Void',txt='Seleccione un item en los comboBox')
            else:
                if self.cbx_1.value == self.cbx_2.value:
                    self.vista.create_dialog(title='Cbx Equal',txt='No se admiten items iguales para el calculo')
                else:
                    self.tb_conting.columns, self.tb_conting.rows = self.fill_table_conting()
                    self.tb_confus.columns, self.tb_confus.rows = self.fill_table_confus()
                    self.fill_lv_conting()
                    self.cal_chi()
                    self.vista.create_dialog(bg=ft.colors.GREEN_ACCENT,bg_btn=ft.colors.GREEN_ACCENT_400,title='Successful',txt='Tablas Calculadas Correctamente')
                    self.page.update()


# Función de arranque del programa.
def main(page:ft.Page):

    dlgVista = DlgVista(page=page, title="Proyecto U2")
    contr = Controlador(page,dlgVista)

    dlgVista.create_window()
    dlgVista.switch_pages()
    contr.init_compontents()
    
    

    page.on_keyboard_event = contr.on_key
    page.theme_mode = ft.ThemeMode.DARK
    page.window.maximizable
    page.update()
 

if __name__ == '__main__':
    ft.app(target=main)
    
