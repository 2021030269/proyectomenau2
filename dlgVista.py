import flet as ft

class DlgVista():
    """
    Esta clase se encarga de todo el apartado visual del programa.

    Métodos
    -------

    __init__(page,width,height,title) 
        Es el contructor de la clase, inicializa todos los componentes visibles

    create_window()
        Crea la ventana con las dimensiones proporcionadas y el titulo

    create_dialog(bg,bg_btn,title,txt)
        Crea ventanas emergentes para realizar avisos o alertas
    
    config_label(size,width,color)
        Devuelve una Configuración el estilo de los labels
    
    config_txt(val,width)
        Devuelve una Configuración el estilo y filtros de los Inputs tipo Texto
    
    create_txt(hint,val,width)
        Devuelve Inputs tipo Texto
    
    create_listView(width,height)
        Devuelve ListViews vacio
    
    create_table(width)
        Devuelve una tabla vacia con dimensiones estaticas
    
    create_calcu_tables()
        Devuelve una tabla vacia con dimensiones dinamicas
    
    create_btn(text,fun=None)
        Devuelve un botón preparado para recibir una función
    
    create_cbx()
        Devuelve un ComboBox vacío
    
    menu_tabs()
        Devuelve un MenuTabs, su función es desplazarse entre a otras ventanas
    
    list_control(*ctrl)
        Devuelve una lista de controladores o componentes de la vista
    
    add_row(*list)
        Devuelve un contenedor row (fila), para agrupar controladores
    
    add_column(*list)
        Devuelve un contenedor column (columna), para agrupar controladores
    
    add_container(contr)
        Devuelve un contenedor, 
        su función es flexibilidad de poscionamiento de otros contenedores o controladores
    
    container_main()
        Devuelve el contenedor princial (inicio)
    
    container_tables()
        Devuelve el contenedor secundario (tablas)
    
    switch_pages()
        Activa el MenuTabs para cambiar de una pesaña a otra
    
    """

    def __init__(self,page=None, width=900,height=600,title='Proyecto Mena'):
        self.page = page
        self.width = width
        self.height = height
        self.title = title 
        # ---------------------- #
        self.lbl_chi = None
        self.lbl_sig95 = None
        self.lbl_sig99 = None
        self.lbl_sig99_99 = None

        self.txt_items = None
        self.txt_inst = None
        self.txt_name = None

        self.lv_item = None
        self.lv_conting = None
        self.lv_confus = None
        
        self.tb_as = None
        self.tb_conting = None
        self.tb_confus = None


        self.btn_manual = None
        self.btn_auto = None
        self.btn_calcular = None
        self.btn_show = None

        self.cbx_items_1 = None
        self.cbx_items_2 = None
    
    def create_window(self):
        self.page.title = self.title
        self.page.window_min_width = self.width
        self.page.window_min_height = self.height
    
    def create_dialog(self,bg=ft.colors.YELLOW_ACCENT,bg_btn=ft.colors.YELLOW_ACCENT_400,title='',txt=''):
        config = self.config_label(color=ft.colors.BLACK)
        dialog = None
        def close_dialog(e):
            dialog.open = False
            self.page.update()

        dialog = ft.AlertDialog(
            title=ft.Text(title,**config,weight=ft.FontWeight.BOLD),
            content=ft.Text(txt,**config),
            bgcolor=bg,
            actions=[ft.TextButton("OK",style=ft.ButtonStyle(
                color=ft.colors.BLACK, 
                bgcolor= {ft.ControlState.DEFAULT: bg_btn}), on_click=close_dialog)]
        )
        self.page.dialog = dialog
        self.page.dialog.open = True
        self.page.update()

    def config_label(self,size=20,width=400,color=ft.colors.BLACK):
        return {
                'size':size,
                'color':color,
                'text_align':ft.TextAlign.CENTER,
                'width':width
            }

    def config_txt(self,val=0,width=150):
        if val==1: 
            return {
                'width':width,
                'color':ft.colors.BLACK,
                'border_color':ft.colors.BLACK,
                'text_size':18,
                'keyboard_type':ft.KeyboardType.NUMBER,
                'input_filter':ft.InputFilter(allow=True, regex_string=r"[0-9]", replacement_string="")
            }
        elif val==2:
            return {
                'width':width,
                'color':ft.colors.BLACK,
                'border_color':ft.colors.BLACK,
                'text_size':12,
                'border':ft.InputBorder.NONE,
                'keyboard_type':ft.KeyboardType.NUMBER,
                'input_filter':ft.InputFilter(allow=True, regex_string=r"[0-1]", replacement_string="")
                
            }
        else:
            return {
                'width':width,
                'color':ft.colors.BLACK,
                'border_color':ft.colors.BLACK,
                'text_size':18
            }

    def create_txt(self,hint,val=0,width=150):
        return ft.TextField(label=hint,hint_text=hint,**self.config_txt(val,width))
    
    def create_listView(self,width=150,height=200):
        lv_items = ft.ListView(expand=2, spacing=10,padding=20,auto_scroll=True)

        lv_cont = ft.Container(
            content=lv_items,
            alignment=ft.alignment.center,
            border= ft.border.all(1,ft.colors.BLACK),
            width=width,
            height=height,
            border_radius=5,
        )

        return lv_items, lv_cont

    def create_table(self,width=600):
        columns = [
                ft.DataColumn(label=ft.Text('')),
                ft.DataColumn(label=ft.Text('')),
                ft.DataColumn(label=ft.Text('')),
            ]

        rows = [
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
        ]

        table = ft.DataTable(
            columns=columns,
            rows=rows,
            width=width

        )

        return table

    def create_calcu_tables(self):
        columns = [
                ft.DataColumn(label=ft.Text('')),
                ft.DataColumn(label=ft.Text('')),
                ft.DataColumn(label=ft.Text('')),
            ]

        rows = [
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
            ft.DataRow(cells=[
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
                ft.DataCell(ft.Text('')),
            ]),
        ]

        table = ft.DataTable(
            columns=columns,
            rows=rows,
        )

        return table

    def create_btn(self,text,fun=None):
        return ft.ElevatedButton(
            text=text,
            bgcolor=ft.colors.WHITE,
            color=ft.colors.BLACK87,
            on_click=fun,
            width=200,
            style=ft.ButtonStyle(shape=ft.ContinuousRectangleBorder(radius=30))
        )

    def create_cbx(self):
        return ft.Dropdown(
            options=[
            ],
            width=200,
        )

    def menu_tabs(self):
        return ft.Tabs(
            selected_index=0,
            animation_duration=300,
            tabs=[
                ft.Tab(
                    text="Inicio",
                    icon=ft.icons.HOME,
                    content=ft.Container(
                        content=self.container_main(),
                        bgcolor=ft.colors.WHITE
                    ),
                ),

                ft.Tab(
                    text="Tablas",
                    icon=ft.icons.TABLE_ROWS,
                    content=ft.Container(
                        content=self.container_tables(),
                        bgcolor=ft.colors.WHITE,
                        padding=50
                        
                    ),
                ),
            ],
            expand=1,
        )
        
    def list_control(self,*ctrl):
        return list(ctrl)
    
    def add_row(self,*list):
        return ft.Row(
            controls=self.list_control(*list),
            alignment=ft.MainAxisAlignment.CENTER,
            spacing=10
        )

    def add_column(self,*list):
        return ft.Column(
            controls=self.list_control(*list),
            alignment=ft.CrossAxisAlignment.CENTER
        )

    def add_container(self,contr):
        return ft.Container(
            content=contr
        )

    def container_main(self):
        #self.page.clean()
        #self.page.add(ft.Row([self.menu_bar()]))
        

        # -------------------------------------------------- #
        self.txt_items = self.create_txt('Items (max:8)',val=1)
        self.txt_inst = self.create_txt('Instancia',val=1)
        row_dim = self.add_row(self.txt_items,self.txt_inst)

        # -------------------------------------------------- #

        self.txt_name = self.create_txt('Item Name')
        self.lv_item, lv_list_item = self.create_listView()
        column_item = self.add_column(self.txt_name,lv_list_item)
        
        self.tb_as = self.create_table()
        cont_tb_as = ft.Container(
            content=ft.Column(
                controls=[self.tb_as],
                scroll=ft.ScrollMode.AUTO
            ),
            alignment=ft.alignment.top_center,
            width=600,
            height=200,
            expand=True
        )

        self.cbx_items_1 = self.create_cbx()
        self.cbx_items_2 = self.create_cbx()
        column_cbx_items = self.add_column(self.cbx_items_1,self.cbx_items_2)

        row_body_item = self.add_row(column_item,cont_tb_as,column_cbx_items)
        # -------------------------------------------------- #

        self.btn_manual = self.create_btn('Manual')
        self.btn_auto = self.create_btn('Automatico')
        self.btn_calcular = self.create_btn('Calcular Tablas')
        self.btn_calcular.disabled = True
        row_btn = self.add_row(self.btn_manual,self.btn_auto,self.btn_calcular)
        cont_row_btn = self.add_container(row_btn)
        cont_row_btn.padding = ft.padding.only(top=30)

        # -------------------------------------------------- #
        return ft.Container(
            content=self.add_column(row_dim, row_body_item,cont_row_btn),
            padding=50,
            theme_mode= ft.ThemeMode.LIGHT
        )

    def container_tables(self):
        #self.page.clean()
        #self.page.add(ft.Row([self.menu_bar()]))

        title_conting = ft.Text('Tabla de Contingencia',**self.config_label(size=30),weight=ft.FontWeight.BOLD)
        self.tb_conting = self.create_table(400)
        self.lv_conting, cont_lv_conting = self.create_listView(width=400,height=260)
        colum_conting = self.add_column(title_conting,self.tb_conting,cont_lv_conting)


        title_confus = ft.Text('Tabla de Frecuencias',**self.config_label(size=30),weight=ft.FontWeight.BOLD)
        self.tb_confus = self.create_table(400)
        #self.lv_confus, cont_lv_confus = self.create_listView(width=400,height=260)
        self.lbl_chi = ft.Text('Chi Value: ',**self.config_label())
        cont_chi = self.add_container(self.lbl_chi)
        cont_chi.padding = ft.padding.only(top=30,bottom=50)
        lbl_sig = ft.Text('Significancia de p-valor',**self.config_label(size=25),weight=ft.FontWeight.BOLD)
        self.lbl_sig95 = ft.Text('95%: ',**self.config_label(width=50))
        self.lbl_sig99 = ft.Text('99%: ',**self.config_label(width=50))
        self.lbl_sig99_99 = ft.Text('99.99%: ',**self.config_label(width=70))
        row_lbl_sig = self.add_row(self.lbl_sig95,self.lbl_sig99,self.lbl_sig99_99)
        row_lbl_sig.width = 400
        row_lbl_sig.alignment = ft.MainAxisAlignment.CENTER


        
        #cont_btn_show_pvalor.border = ft.border.all(1,ft.colors.BLACK)
        colum_confus = self.add_column(title_confus,self.tb_confus,cont_chi,lbl_sig,row_lbl_sig)

        row_container = self.add_row(colum_conting,colum_confus)
        row_container.alignment = ft.MainAxisAlignment.SPACE_BETWEEN

        return ft.Container(
            content=row_container,
            padding=ft.padding.only(top=-20),
        )

    def switch_pages(self):
        self.page.add(self.menu_tabs())
  

           

